﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Orgatron.Models
{

    public enum EtatInvitation
    {
        [Description("Par défaut : accepte la partie")]
        Accepted,
        [Description("A refusé")]
        Rejected
    }

    public class Participant
    {
        public int Id { get; set; }
        public Partie Partie { get; set; }
        public IdentityUser User { get; set; }
        public string Equipe { get; set; }
        public string Notes { get; set; }
        public int Points { get; set; }
        public EtatInvitation Etat { get; set; }
    }
}