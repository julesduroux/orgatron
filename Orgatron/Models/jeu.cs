﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Orgatron.Models
{
    public class Jeu
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        [DataType(DataType.MultilineText)]
        public string Regles { get; set; }
        public string Description { get; set; }
        public int NbEquipesMin { get; set; }
        public int NbEquipesMax { get; set; }
        public int NbParticipantsParEquipesMin { get; set; }
        public int NbParticipantsParEquipesMax { get; set; }
        public int DureeIndicative { get; set; }
        public bool EstActif { get; set; }
    }
}