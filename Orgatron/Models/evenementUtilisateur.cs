﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Orgatron.Models
{
    public class EvenementUtilisateur
    {
        public int Id { get; set; }
        public Evenement Evenement { get; set; }
        public IdentityUser User { get; set; }
        public bool EstEnPause { get; set; }
        public bool EstModerateur { get; set; }
        public DateTime DateDebutPause { get; set; }
        public double NbSecondesPause { get; set; }
        public int ModificateurPoints { get; set; }
    }
}