﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class addListofelementsinparentelement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "nbPersonnesEnAttente",
                table: "Evenement",
                newName: "NbPersonnesEnAttente");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NbPersonnesEnAttente",
                table: "Evenement",
                newName: "nbPersonnesEnAttente");
        }
    }
}
