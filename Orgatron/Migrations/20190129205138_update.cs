﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Equipe_Evenement_EvenementID",
                table: "Equipe");

            migrationBuilder.DropForeignKey(
                name: "FK_EvenementUtilisateur_Evenement_EvenementID",
                table: "EvenementUtilisateur");

            migrationBuilder.RenameColumn(
                name: "EvenementID",
                table: "EvenementUtilisateur",
                newName: "EvenementId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "EvenementUtilisateur",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_EvenementUtilisateur_EvenementID",
                table: "EvenementUtilisateur",
                newName: "IX_EvenementUtilisateur_EvenementId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Evenement",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "EvenementID",
                table: "Equipe",
                newName: "EvenementId");

            migrationBuilder.RenameIndex(
                name: "IX_Equipe_EvenementID",
                table: "Equipe",
                newName: "IX_Equipe_EvenementId");

            migrationBuilder.CreateTable(
                name: "PropositionPremade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HebergeurId = table.Column<string>(nullable: true),
                    InviteId = table.Column<string>(nullable: true),
                    EstValide = table.Column<bool>(nullable: false),
                    EvenementId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropositionPremade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropositionPremade_Evenement_EvenementId",
                        column: x => x.EvenementId,
                        principalTable: "Evenement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropositionPremade_AspNetUsers_HebergeurId",
                        column: x => x.HebergeurId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropositionPremade_AspNetUsers_InviteId",
                        column: x => x.InviteId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PropositionPremade_EvenementId",
                table: "PropositionPremade",
                column: "EvenementId");

            migrationBuilder.CreateIndex(
                name: "IX_PropositionPremade_HebergeurId",
                table: "PropositionPremade",
                column: "HebergeurId");

            migrationBuilder.CreateIndex(
                name: "IX_PropositionPremade_InviteId",
                table: "PropositionPremade",
                column: "InviteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Equipe_Evenement_EvenementId",
                table: "Equipe",
                column: "EvenementId",
                principalTable: "Evenement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EvenementUtilisateur_Evenement_EvenementId",
                table: "EvenementUtilisateur",
                column: "EvenementId",
                principalTable: "Evenement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Equipe_Evenement_EvenementId",
                table: "Equipe");

            migrationBuilder.DropForeignKey(
                name: "FK_EvenementUtilisateur_Evenement_EvenementId",
                table: "EvenementUtilisateur");

            migrationBuilder.DropTable(
                name: "PropositionPremade");

            migrationBuilder.RenameColumn(
                name: "EvenementId",
                table: "EvenementUtilisateur",
                newName: "EvenementID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "EvenementUtilisateur",
                newName: "ID");

            migrationBuilder.RenameIndex(
                name: "IX_EvenementUtilisateur_EvenementId",
                table: "EvenementUtilisateur",
                newName: "IX_EvenementUtilisateur_EvenementID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Evenement",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "EvenementId",
                table: "Equipe",
                newName: "EvenementID");

            migrationBuilder.RenameIndex(
                name: "IX_Equipe_EvenementId",
                table: "Equipe",
                newName: "IX_Equipe_EvenementID");

            migrationBuilder.AddForeignKey(
                name: "FK_Equipe_Evenement_EvenementID",
                table: "Equipe",
                column: "EvenementID",
                principalTable: "Evenement",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EvenementUtilisateur_Evenement_EvenementID",
                table: "EvenementUtilisateur",
                column: "EvenementID",
                principalTable: "Evenement",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
