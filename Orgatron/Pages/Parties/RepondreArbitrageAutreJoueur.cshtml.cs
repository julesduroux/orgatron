﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Parties
{
    public class RepondreArbitrageAutreJoueurModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PartieService _partieService;
        private string _message;
        private readonly IAuthorizationService _authorizationService;

        public RepondreArbitrageAutreJoueurModel(OrgatronContext context, UserManager<IdentityUser> userManager, PartieService partieService, IAuthorizationService authorizationService)
        {
            _context = context;
            _userManager = userManager;
            _partieService = partieService;
            _authorizationService = authorizationService;
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public string IdPartie { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [BindProperty]
            [Required]
            public bool Comming { get; set; }
        }

        public IActionResult OnGet(int? idPartie)
        {
            IdPartie = idPartie.ToString();
            Input = new InputModel
            {
                Comming = true
            };

            return Page();
        }

        public async System.Threading.Tasks.Task<IActionResult> OnPostAsync(int? idPartie)
        {

            IdPartie = idPartie.ToString();

            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (idPartie == null)
            {
                return NotFound();
            }

            Partie invitation = _context.Partie.Include(p => p.Arbitre).Include(p => p.Evenement).Where(p => p.Id == idPartie).FirstOrDefault();

            bool passwordIsGood = (await _authorizationService.AuthorizeAsync(User, "AdminOnly")).Succeeded || await _userManager.CheckPasswordAsync(invitation.Arbitre, Input.Password);

            bool success = false;

            if (passwordIsGood)
            {
                success = _partieService.RepondreArbitrage(invitation, _context, EtatInvitation.Rejected, out _message);
            }
            else
            {
                Message = "Le mot de passe est incorrect";
            }

            if (success)
            {
                return RedirectToPage("/Parties/Details", new { invitation.Id });
            }
            return Page();
        }
    }
}
