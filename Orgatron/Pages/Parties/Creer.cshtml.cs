﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Parties
{
    public class CreateModel : PageModel
    {
        private readonly OrgatronContext _context;
        public readonly IAuthorizationService _authorizationService;

        public CreateModel(OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        [BindProperty]
        public InputPartieModel InputPartie { get; set; }
        public class InputPartieModel
        {
            public string EvenementId { get; set; }
            public string UserId { get; set; }
            public string JeuId { get; set; }
            public DateTime DateDebut { get; set; }
            public DateTime DateFin { get; set; }
            public string EtatPartie { get; set; }
        }

        public IEnumerable<SelectListItem> ListeJoueurs { get; set; }
        public IEnumerable<SelectListItem> ListeJeux { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Evenement evenement = await _context.Evenement.Include(p => p.Createur).FirstOrDefaultAsync(m => m.Id == id);

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, evenement, "EstCreateurEvenement");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }


            ListeJoueurs = _context.Participant.Include(p => p.User).Where(p => p.Partie.Evenement == evenement).Select(i => new SelectListItem()
            {
                Text = i.User.UserName,
                Value = i.User.Id
            });

            ListeJeux = _context.Jeu.Where(j => j.EstActif == true).Select(i => new SelectListItem()
            {
                Text = i.Nom,
                Value = i.Id.ToString()
            });

            return Page();
        }

        public async Task<IActionResult> OnPostAddPartieAsync()
        {
            Evenement evenement = await _context.Evenement.Include(p => p.Createur).FirstOrDefaultAsync(m => m.Id == Int32.Parse(InputPartie.EvenementId));

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, evenement, "EstCreateurEvenement");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            Partie partie = new Partie
            {
                Arbitre = _context.Users.Find(InputPartie.UserId),
                EtatArbitre = EtatInvitation.Accepted,
                DateDebut = DateTime.Now,
                DateFin = DateTime.MinValue,
                Etat = (EtatPartie)Int32.Parse(InputPartie.EtatPartie),
                Jeu = _context.Jeu.Find(Int32.Parse(InputPartie.JeuId)),
                //Pour identifier les parties créées manuellement
                DateCreation = DateTime.MinValue,
                Evenement = evenement
            };

            _context.Partie.Add(partie);

            _context.SaveChanges();

            return RedirectToPage("/Parties/Modifier", new { id = partie.Id });
        }

    }
}
