﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Parties
{
    public class ScoreModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly PartieService _partieService;

        public ScoreModel(OrgatronContext context, IAuthorizationService authorizationService, PartieService partieService)
        {
            _context = context;
            _authorizationService = authorizationService;
            _partieService = partieService;
        }

        [BindProperty]
        public InputModel Equipes { get; set; }

        public string ErreurClassement { get; set; }

        public class InputModel
        {
            public int PartieId { get; set; }
            public Partie Partie { get; set; }
            public List<InputEquipeModel> ListeEquipe { get; set; }
            [DataType(DataType.MultilineText)]
            public string Notes { get; set; }
            public Jeu Jeu { get; set; }
            public string Chrono { get; set; }
        }

        public class InputEquipeModel
        {
            public string Name { get; set; }
            public string Participants { get; set; }
            [Required]
            public int Points { get; set; }
            public string Notes { get; set; }
            public int NbParticipants { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Partie partie = await _context.Partie.Include(p => p.Jeu).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);
            partie.Participants = partie.Participants.Where(p => p.Etat == EtatInvitation.Accepted).ToList();

            if (partie == null)
            {
                return NotFound();
            }

            Equipes = new InputModel
            {
                Notes = partie.NotesArbitre,
                PartieId = partie.Id,
                Partie = partie,
                Jeu = partie.Jeu,
                Chrono = partie.EtatChrono == EtatChrono.Paused ? (partie.DateDebutPause - partie.DateDebutChrono.AddSeconds(partie.NbSecondesPauseChrono)).ToString("h'h 'm'm 's's'") : (DateTime.Now - partie.DateDebutChrono.AddSeconds(partie.NbSecondesPauseChrono)).ToString("h'h 'm'm 's's'")
            };

            List<string> equipesAjoutees = new List<string>();
            Equipes.ListeEquipe = new List<InputEquipeModel>();
            foreach (var joueur in partie.Participants)
            {
                if (!equipesAjoutees.Contains(joueur.Equipe))
                {
                    Equipes.ListeEquipe.Add(new InputEquipeModel { Name = joueur.Equipe, Points = joueur.Points, Notes = joueur.Notes, Participants = joueur.User.UserName, NbParticipants = 1 });
                    equipesAjoutees.Add(joueur.Equipe);
                }
                else
                {
                    Equipes.ListeEquipe.Where(e => e.Name == joueur.Equipe).FirstOrDefault().Participants += String.Format(", {0}", joueur.User.UserName);
                    Equipes.ListeEquipe.Where(e => e.Name == joueur.Equipe).FirstOrDefault().NbParticipants += 1;
                }
            }
            

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            //récupérer la partie en question en base et ne modifier que les scores des participants
            Partie partieEnBDD = _context.Partie.Include(p => p.Evenement).Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Participants).Where(p => p.Id == Equipes.PartieId).FirstOrDefault();
            //On garde les refus dans un coin pour ne pas les perde à la sauvegarde
            List<Participant> refus = partieEnBDD.Participants.Where(p => p.Etat == EtatInvitation.Rejected).ToList();
            partieEnBDD.Participants = partieEnBDD.Participants.Where(p => p.Etat == EtatInvitation.Accepted).ToList();
            //valider que c'est bien quelqu'un qui a le droit de modifier la partie.

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, partieEnBDD, "EstArbitrePartie");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            //Au cas où il y ait des erreurs
            Equipes.Partie = partieEnBDD;

            if (!ModelState.IsValid)
            {
                return Page();
            }

            int totalPoints = 0;
            foreach (var equipe in Equipes.ListeEquipe)
            {
                if (equipe.Points > 3)
                {
                    ModelState.AddModelError(string.Empty, "Une équipe ne peut pas avoir plus de 3 points");
                    return Page();
                }
                if (equipe.Points < 0)
                {
                    ModelState.AddModelError(string.Empty, "Des points négatifs ? Seriously ?!!");
                    return Page();
                }
                totalPoints += equipe.Points * equipe.NbParticipants;
            }

            if (totalPoints > 6)
            {
                ModelState.AddModelError(string.Empty, "Tu es sûr de ton coup ? Tu as donné trop de points.");
                return Page();
            }

            foreach (Participant participant in partieEnBDD.Participants)
            {
                participant.Points = Equipes.ListeEquipe.Where(le => le.Name == participant.Equipe).FirstOrDefault().Points;
                participant.Notes = Equipes.ListeEquipe.Where(le => le.Name == participant.Equipe).FirstOrDefault().Notes;
            }


            partieEnBDD.NotesArbitre = Equipes.Notes;
            if (partieEnBDD.EtatChrono == EtatChrono.Created)
            {
                partieEnBDD.DateDebutChrono = DateTime.Now;
            }
            if (partieEnBDD.EtatChrono != EtatChrono.Created && partieEnBDD.EtatChrono != EtatChrono.Paused)
            {
                partieEnBDD.EtatChrono = EtatChrono.Paused;
                partieEnBDD.DateDebutPause = DateTime.Now;
            }
            if (partieEnBDD.Etat != EtatPartie.Cancelled && partieEnBDD.Etat != EtatPartie.Ended)
            {
                partieEnBDD.DateFin = DateTime.Now;
            }
            partieEnBDD.Etat = EtatPartie.Ended;


            //On réinjecte les parties rejetées
            foreach (var refu in refus)
            {
                partieEnBDD.Participants.Add(refu);
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartieExists(Equipes.PartieId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            _partieService.LancerParties(partieEnBDD.Evenement, _context);

            //Mise à jour de la valeur pour l'affichage de la page
            partieEnBDD.Participants = partieEnBDD.Participants.Where(p => p.Etat == EtatInvitation.Accepted).ToList();
            Equipes.Partie = partieEnBDD;


            return RedirectToPage("/Parties/Details", new { partieEnBDD.Id });
        }

        public async Task<IActionResult> OnPostCancelGameAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            Partie partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);

            if (partie == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            if (partie.Etat != EtatPartie.Cancelled && partie.Etat != EtatPartie.Ended)
            {
                partie.DateFin = DateTime.Now;
            }
            partie.Etat = EtatPartie.Cancelled;
            if (partie.EtatChrono != EtatChrono.Created && partie.EtatChrono != EtatChrono.Paused)
            {
                partie.EtatChrono = EtatChrono.Paused;
                partie.DateDebutPause = DateTime.Now;
            }
            
            _context.SaveChanges();

            //On regarde s'il ne faut pas relancer une partie après en avoir annulé une
            _partieService.LancerParties(partie.Evenement, _context);

            return RedirectToPage("/Parties/Details", new { partie.Id });
        }

        private bool PartieExists(int id)
        {
            return _context.Partie.Any(e => e.Id == id);
        }
    }
}
