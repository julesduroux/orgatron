﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Evenements
{
    public class ModerateurModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IAuthorizationService _authorizationService;

        public ModerateurModel(OrgatronContext context, UserManager<IdentityUser> userManager, IAuthorizationService authorizationService)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
        }


        public async System.Threading.Tasks.Task<IActionResult> OnGetAsync(int? IdEvenement, string playerName, bool? value)
        {

            IdentityUser utilisateur = await _userManager.FindByNameAsync(playerName);
            if (IdEvenement == null || utilisateur == null || value == null)
            {
                return NotFound();
            }
            EvenementUtilisateur evenementUtilisateur = _context.EvenementUtilisateur.Include(e => e.Evenement).Include(e => e.Evenement.Createur).Where(e => e.Evenement.Id == IdEvenement.Value && e.User.Id == utilisateur.Id).FirstOrDefault();

            bool autorisation = (await _authorizationService.AuthorizeAsync(User, evenementUtilisateur.Evenement, "EstCreateurEvenement")).Succeeded;
            if (!autorisation)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            if (value.Value == true)
            {
                await _userManager.AddClaimAsync(utilisateur, new Claim("Admin", "Admin"));
                evenementUtilisateur.EstModerateur = true;
            }
            else
            {
                await _userManager.RemoveClaimAsync(utilisateur, new Claim("Admin", "Admin"));
                evenementUtilisateur.EstModerateur = false;
            }

        _context.SaveChanges();
            
            return RedirectToPage("/Evenements/Invitation", new { id = IdEvenement });
        }
    }
}
