﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    [Authorize(Policy = "AdminOnly")]
    public class DeleteModel : PageModel
    {
        private readonly Orgatron.Models.OrgatronContext _context;

        public DeleteModel(Orgatron.Models.OrgatronContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Evenement Evenement { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Evenement = await _context.Evenement.FirstOrDefaultAsync(m => m.Id == id);

            if (Evenement == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Evenement = await _context.Evenement.FindAsync(id);

            if (Evenement != null)
            {
                _context.Evenement.Remove(Evenement);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
