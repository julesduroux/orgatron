﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Evenements
{
    [Authorize(Policy = "AdminOnly")]
    public class StartModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PartieService _partieService;

        public StartModel(OrgatronContext context, UserManager<IdentityUser> userManager, PartieService partieService)
        {
            _context = context;
            _userManager = userManager;
            _partieService = partieService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Evenement evenement = _context.Evenement.Where(e => e.Id == id).FirstOrDefault();

            if (evenement != null && evenement.StartedAt == DateTime.MinValue)
            {
                evenement.StartedAt = DateTime.Now;
                _context.SaveChanges();
            }

            _partieService.LancerParties(evenement, _context);

            return RedirectToPage("./Index", new { id });
        }
    }
}