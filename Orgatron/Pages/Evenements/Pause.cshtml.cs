﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Evenements
{
    public class PauseModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IAuthorizationService _authorizationService;

        public PauseModel(OrgatronContext context, UserManager<IdentityUser> userManager, IAuthorizationService authorizationService)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
        }


        public async System.Threading.Tasks.Task<IActionResult> OnGetAsync(int? IdEvenement, string playerName, bool? value)
        {

            IdentityUser utilisateur = await _userManager.FindByNameAsync(playerName);
            if (IdEvenement == null || utilisateur == null || value == null)
            {
                return NotFound();
            }
            
            bool autorisation = (await _authorizationService.AuthorizeAsync(User, "AdminOnly")).Succeeded || utilisateur.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (!autorisation)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            EvenementUtilisateur evenementUtilisateur = _context.EvenementUtilisateur.Where(e => e.Evenement.Id == IdEvenement.Value && e.User.Id == utilisateur.Id).FirstOrDefault();
            if (evenementUtilisateur.EstEnPause)
            {
                evenementUtilisateur.NbSecondesPause += (DateTime.Now - evenementUtilisateur.DateDebutPause).TotalSeconds;
            }
            else
            {
                evenementUtilisateur.DateDebutPause = DateTime.Now;
            }
            evenementUtilisateur.EstEnPause = value.Value;

        _context.SaveChanges();
            
            return RedirectToPage("/Evenements/Home", new { id = IdEvenement });
        }
    }
}
