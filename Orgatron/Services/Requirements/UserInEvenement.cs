﻿// requirement class
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Orgatron.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

public class UserInEvenementRequirement : IAuthorizationRequirement { }

// handler class
public class UserInEvenementAuthorizationHandler : AuthorizationHandler<UserInEvenementRequirement, EvenementUtilisateur>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserInEvenementRequirement requirement, EvenementUtilisateur evenementUtilisateur)
    {
        if (context.User == null || evenementUtilisateur == null)
        {
            return Task.CompletedTask;
        }

        if (evenementUtilisateur.User.Id == context.User.FindFirst(ClaimTypes.NameIdentifier).Value)
        {
            context.Succeed(requirement);
        }
        
        return Task.CompletedTask;
    }

}