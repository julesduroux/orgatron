﻿// requirement class
using Microsoft.AspNetCore.Authorization;
using Orgatron.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

public class EstArbitrePartieRequirement : IAuthorizationRequirement { }

// handler class
public class EstArbitrePartieAuthorizationHandler : AuthorizationHandler<EstArbitrePartieRequirement, Partie>
{

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, EstArbitrePartieRequirement requirement, Partie partie)
    {
        if (context.User == null || partie == null)
        {
            return Task.CompletedTask;
        }

        bool estArbitre = partie.Arbitre.Id == context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
        bool estAdmin = context.User.HasClaim(c => c.Value == "Admin");

        if (estAdmin || (estArbitre && (partie.Etat == EtatPartie.Created || partie.Etat == EtatPartie.Started)))
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }

}