﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orgatron.Models;

[assembly: HostingStartup(typeof(Orgatron.Areas.Identity.IdentityHostingStartup))]
namespace Orgatron.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {

                services.AddDefaultIdentity<IdentityUser>()
                    .AddEntityFrameworkStores<OrgatronContext>();
            });
        }
    }
}